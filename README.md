# @ndmspc/react-ndmspc-core

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/@ndmspc/react-ndmspc-core.svg)](https://www.npmjs.com/package/@ndmspc/react-ndmspc-core) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @ndmspc/react-ndmspc-core
```

## Usage

```jsx
import React, { useEffect, useLayoutEffect, useState } from 'react'
import { version } from 'jsroot'

import {
  localStore,
  redirectStore,
  NdmSpcContext,
  executorStore,
  Distributor,
  broker,
  WebSocketStreamBroker
} from './lib'
import LocalStoreInfoExample from './components/LocalStoreInfoExample'
import RedirectExample from './components/RedirectExample'
import HttpExample from './components/HttpExample'
import ExecutorInfoExample from './components/ExecutorStoreExample'
import DistributorExample from './components/DistributorExample'
import HistogramFunctionsExample from './components/HistogramFunctionsExample'
import StreamBrokerExample from './components/StreamBrokerExample'
import WebSocketConnectForm from './components/WebSocketConnectForm'

let store = {
  local: localStore('localOne'),
  localSecond: localStore('localTwo'),
  zmq2wsUrl: localStore('zmq2wsUrl'),
  sharedUrl: localStore('sharedUrl'),
  redirect: redirectStore(),
  ws: {
    app: redirectStore(),
    ws: redirectStore()
  },
  http: redirectStore(),
  executor: executorStore('executor'),
  salsaDistributor: new Distributor('sls'),
  projDistributor: new Distributor('proj'),
  viewDistributor: new Distributor('view'),
  configDistributor: new Distributor('cnf')
}

broker.state = {
  counter: 0,
  click: '',
  dbClick: '',
  hover: ''
}

// add subjects to broker
broker.addDistributor(store.projDistributor)
broker.addDistributor(store.salsaDistributor)
broker.addDistributor(store.viewDistributor)
broker.addDistributor(store.configDistributor)

// add handlers for subjects
broker.addHandlerFunc('sls', (dataFromSalsa) => {
  console.log(`Started Job`)

  setTimeout(() => {
    console.log(`Job Finished`)
    const counter = broker.state.counter ? broker.state.counter : 1
    broker.state = { ...broker.state, counter: counter + 1 }
    store.configDistributor.sendInputData('incremented')
    return dataFromSalsa
  }, 5000)
})

broker.addHandlerFunc(
  'view',
  null,
  (dataFromBrowser) => {
    console.log(`NdmBatchSystemComponent: ${dataFromBrowser.data}`)
    if (dataFromBrowser.event === 'CONTEXT') {
      broker.state[dataFromBrowser.data.operation] =
        dataFromBrowser.data.projName
    }
  },
  null
)

broker.addHandlerFunc(
  'cnf',
  null,
  (dataFromConfig) => {
    console.log(`NdmBatchSystemComponent: ${dataFromConfig.data}`)
  },
  null
)

store.userFunctions = {
  onClick: (data) => {
    if (broker.state.click) {
      const newData = {
        binx: data.binx,
        biny: data.biny,
        projName: broker.state.click
      }
      store.projDistributor.sendInputData(newData)
    }
  },
  onHover: (data) => {
    if (data) {
      if (broker.state.hover) {
        const newData = {
          binx: data.binx,
          biny: data.biny,
          projName: broker.state.hover
        }
        store.projDistributor.sendInputData(newData)
      }
    }
  },
  onDbClick: (data) => {
    if (broker.state.dbClick) {
      const newData = {
        binx: data.binx,
        biny: data.biny,
        projName: broker.state.dbClick
      }
      store.projDistributor.sendInputData(newData)
    }
  }
}

const App = () => {
  return (
    <>
      {version && <h2>JSROOT Version: {version}</h2>}
      <NdmSpcContext.Provider
        value={{
          zmq2ws: WebSocketStreamBroker(),
          shared: WebSocketStreamBroker()
        }}
      >
        <WebSocketConnectForm store={store.zmq2wsUrl} wssb='zmq2ws'/>
        <StreamBrokerExample wssb='zmq2ws'/>
        <WebSocketConnectForm store={store.sharedUrl} wssb='shared'/>
        <StreamBrokerExample wssb='shared'/>
      </NdmSpcContext.Provider>
      
      <LocalStoreInfoExample store={store.local} delay='1000' />
      <LocalStoreInfoExample store={store.localSecond} delay='1500' />
      <ExecutorInfoExample store={store.executor} />
      <RedirectExample store={store.redirect} />
      <HttpExample store={store} />
      <DistributorExample store={store} />
      <HistogramFunctionsExample />
    </>
  )
}

export default App
```

## License

MIT © [ndmspc](https://gitlab.com/ndmspc)
