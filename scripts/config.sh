#!/bin/bash
export NDMSPC_MODE=${NDMSPC_MODE-"development"}
export WS_URL=${WS_URL-"ws://localhost:8442"}

PROJECT_DIR="$(dirname $(dirname $(readlink -m $0)))"
export NDMSPC_APP_BASE_DIR=${NDMSPC_APP_BASE_DIR-$PROJECT_DIR/example/build}


cat <<EOF > $NDMSPC_APP_BASE_DIR/env.$NDMSPC_MODE.js
window.env = {
  // This option can be retrieved in "src/index.js" with "window.env.API_URL".
  WS_URL: "$WS_URL"
};
EOF
