import { AppGlobalScope, Distributor, executorStore, localStore, redirectStore } from './lib'

const broker = new AppGlobalScope({
  counter: 0,
  click: '',
  dbClick: '',
  hover: ''
})

let store = {
  local: localStore('localOne'),
  localSecond: localStore('localTwo'),
  zmq2wsUrl: localStore('zmq2wsUrl'),
  sharedUrl: localStore('sharedUrl'),
  redirect: redirectStore(),
  ws: {
    app: redirectStore(),
    ws: redirectStore()
  },
  http: redirectStore(),
  executor: executorStore('executor'),
  salsaDistributor: new Distributor('sls'),
  projDistributor: new Distributor('proj'),
  viewDistributor: new Distributor('view'),
  configDistributor: new Distributor('cnf')
}

// add subjects to broker
broker.addDistributor(store.projDistributor)
broker.addDistributor(store.salsaDistributor)
broker.addDistributor(store.viewDistributor)
broker.addDistributor(store.configDistributor)

broker.addHandlerFunc('sls', (dataFromSalsa) => {
  console.log(`Started Job`)

  setTimeout(() => {
    console.log(`Job Finished`)
    const counter = broker.state.counter ? broker.state.counter : 1
    broker.state = { ...broker.state, counter: counter + 1 }
    store.configDistributor.sendInputData('incremented')
    return dataFromSalsa
  }, 5000)
})

broker.addHandlerFunc(
  'view',
  null,
  (dataFromBrowser) => {
    console.log(`NdmBatchSystemComponent: ${dataFromBrowser.data}`)
    if (dataFromBrowser.event === 'CONTEXT') {
      broker.state[dataFromBrowser.data.operation] =
        dataFromBrowser.data.projName
    }
  },
  null
)

broker.addHandlerFunc(
  'cnf',
  null,
  (dataFromConfig) => {
    console.log(`NdmBatchSystemComponent: ${dataFromConfig.data}`)
  },
  null
)

store.userFunctions = {
  onClick: (data) => {
    if (broker.state.click) {
      const newData = {
        binx: data.binx,
        biny: data.biny,
        projName: broker.state.click
      }
      store.projDistributor.sendInputData(newData)
    }
  },
  onHover: (data) => {
    if (data) {
      if (broker.state.hover) {
        const newData = {
          binx: data.binx,
          biny: data.biny,
          projName: broker.state.hover
        }
        store.projDistributor.sendInputData(newData)
      }
    }
  },
  onDbClick: (data) => {
    if (broker.state.dbClick) {
      const newData = {
        binx: data.binx,
        biny: data.biny,
        projName: broker.state.dbClick
      }
      store.projDistributor.sendInputData(newData)
    }
  }
}

export { broker, store }

