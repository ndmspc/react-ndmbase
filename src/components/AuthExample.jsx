import React, { useLayoutEffect } from 'react'

import { useAuth } from '../lib'

const AuthExample = () => {
  const [isLogin, token] = useAuth();
  useLayoutEffect(() => {
    console.log("AuthExample::token", token)
  }, [token])
  return isLogin ? <div >Logged in as {token}</div> : <div >Please login first</div>;
}

export default AuthExample
