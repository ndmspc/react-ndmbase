import React, { useEffect } from 'react'
import { openFile, redraw } from 'jsroot'
import { createTH1Projection } from '../lib'

const HistogramFunctionsExample = ({
  filename = 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root'
}) => {
  // extending the function
  const extendedOnBinClick = async (data) => {
    if (data) {
      await createTH1Projection('X', data, ['gitlab2'], ['X', 'Y'])
      await createTH1Projection('Y', data, ['gitlab3'], ['X', 'Y'])
    }
  }

  const drawHistogram = async () => {
    const file = await openFile(filename)
    const hist = await file.readObject('hUsersVsProjects')
    const painter = await redraw('gitlab1', hist, 'colz')
    if (painter != null) {
      painter.configureUserClickHandler(extendedOnBinClick)
    }
  }

  useEffect(() => {
    drawHistogram()
  }, [filename])

  const style = {
    display: 'flex',
    flexDirection: 'row-reverse',
    margin: '1rem',
    padding: '1rem',
    border: '2px solid black'
  }

  const projectionStyle = {
    width: '450px',
    height: '400px',
    background: 'grey',
    marginLeft: '1rem'
  }

  return (
    <div style={style}>
      <div id='gitlab1' style={projectionStyle}></div>
      <div id='gitlab2' style={projectionStyle}></div>
      <div id='gitlab3' style={projectionStyle}></div>
    </div>
  )
}

export default HistogramFunctionsExample
