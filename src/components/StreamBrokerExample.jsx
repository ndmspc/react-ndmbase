import React, { useContext, useLayoutEffect, useState } from 'react'
import { JsonViewer } from '@textea/json-viewer'
import { useStreamBrokerIn, NdmSpcContext } from '../lib'

const StreamBrokerExample = ({wssb = 'zmq2ws'}) => {
  const sb = useContext(NdmSpcContext)[wssb]
  const [subscribed, setSubscribed] = useState(false)
  const app = useStreamBrokerIn('app', wssb)
  const ws = useStreamBrokerIn('ws', wssb)
  const zmq = useStreamBrokerIn('zmq', wssb)

  useLayoutEffect(() => {
    console.log(sb)
    if (!sb.isConnected) setSubscribed(false)
  }, [sb])

  function handleSubscription() {
    if (sb == null) return
    if (app == null) return
    const { name, sub, src } = app.payload.services[0]
    if (!subscribed) {
      sb.send({
        type: 'sub',
        name,
        sub,
        src
      })
    } else {
      sb.send({
        type: 'unsub',
        name,
        sub,
        src
      })
    }
    setSubscribed(!subscribed)
  }

  return (
    <>
      <JsonViewer key='app' value={app} />
      <JsonViewer key='ws' value={ws} />
      <JsonViewer key='zmq' value={zmq} />
      <button onClick={handleSubscription}>
        {!subscribed ? 'Subscribe' : 'Unsubscribe'}
      </button>
    </>
  )
}

export default StreamBrokerExample
