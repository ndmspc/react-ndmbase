import React, { useContext, useLayoutEffect, useState } from 'react'
import { NdmSpcContext, useLocalStore, useStreamBrokerOut } from '../lib'

const WebSocketConnectForm = ({ store, wssb = 'zmq2ws' }) => {
  const sb = useContext(NdmSpcContext)[wssb]
  const [url, setUrl] = useState('wss://zmq2ws.upjs.ndmspc.io:443')
  const zmq2wsUrl = useLocalStore(store)
  const [connected, setConnected] = useState(false)
  const [loading, setLoading] = useState(false)

  const _ws = useStreamBrokerOut('_ws',wssb)

  useLayoutEffect(() => {
    if (_ws.type === '_ws') {
      if (_ws.action === 'state') {
        setConnected(_ws.payload.connected)
        setLoading(_ws.payload.loading)
      }
    }
  }, [_ws])

  useLayoutEffect(() => {
    if (zmq2wsUrl?.data?.url) {
      setUrl(zmq2wsUrl.data.url)
    }
  }, [zmq2wsUrl])

  const handleUrl = (event) => {
    setUrl(event.target.value)
  }

  const onSubmitUrl = (event) => {
    event.preventDefault()
    const url = event.target.elements.url.value
    store.setData({ url: url })
    if (sb === null) return
    if (!connected) {
      sb.connect(url)
    } else {
      sb.disconnect()
    }
  }

  return (
    <>
      <form onSubmit={onSubmitUrl}>
        <input
          type='text'
          size='64'
          id='url'
          name='url'
          value={url}
          onChange={handleUrl}
          disabled={connected ? 'disabled' : null}
        />
        <button type='submit' disabled={loading ? 'disabled' : null}>
          {connected ? 'Disconnect' : loading ? 'Connecting ...' : 'Connect'}
        </button>
      </form>
    </>
  )
}

export default WebSocketConnectForm
