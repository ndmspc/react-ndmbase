import React from 'react'
import { JsonViewer } from '@textea/json-viewer'
import { useRedirectStore } from '../lib'

const WebSocketExample = ({ store }) => {
  const app = useRedirectStore(store.app)
  console.log(app)
  const ws = useRedirectStore(store.ws)
  return (
    <>
      <JsonViewer key='app' value={app} />
      <JsonViewer key='ws' value={ws} />
    </>
  )
}

export default WebSocketExample
