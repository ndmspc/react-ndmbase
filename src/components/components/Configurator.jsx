import React, { useEffect, useState } from 'react'
import { broker } from '../../AppGlobalContext'

const Configurator = ({ store }) => {

  const [counter, setCounter] = useState(null)
  const salsaDistributor = store.salsaDistributor

  useEffect(() => {
    const subscription = broker.createExternalSubscription(
      'cnf',
      (toUpdateComponent) => {
        if (toUpdateComponent === 'incremented'){
          setCounter(broker.state.counter)
        }
      }
    )
    return () => subscription.unsubscribe()
  }, [])

  return (
    <div style={{ width: '20%' }}>
      <div>CONFIGURATOR {counter}</div>
      <div>
        <button onClick={() => salsaDistributor.sendInputData('config1')
        }>Config1
        </button>
      </div>
      <div>
        <button onClick={() => salsaDistributor.sendInputData('config2')}>Config2</button>
      </div>
      <div>
        <button onClick={() => salsaDistributor.sendInputData('config3')}>Config3</button>
      </div>
    </div>
  )
}

export default Configurator
