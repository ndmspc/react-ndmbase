import React, { useEffect } from 'react'
import { readProjectionFromFile } from '../../lib'
import { broker } from '../../AppGlobalContext'

const Projection = ({ filename = 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/test-results-final-out.root' }) => {

  useEffect(() => {
    const subscription = broker.createExternalSubscription(
      'proj',
      async (toUpdateComponent) => {
        console.log('component: Projection')
        console.log(toUpdateComponent)
        const data = toUpdateComponent
        await readProjectionFromFile(filename, 'pt_vs_mult/bins', data.binx, data.biny, data.projName, ['proj2'])
      }
    )
    return () => subscription.unsubscribe()
  }, [filename])

  const projectionStyle = {
    width: '400px',
    height: '400px',
    background: 'grey',
    marginLeft: '1rem'
  }

  return (
    <div id='proj2' style={projectionStyle}></div>
  )
}

export default Projection
