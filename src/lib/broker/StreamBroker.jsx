import { redirectStore } from '../store/redirect'
import { filter } from 'rxjs/operators'

const initialState = {}
export class StreamBroker {
  storeIn = null
  storeOut = null

  constructor(cache) {
    this.storeIn = redirectStore(cache) // from websocket
    this.storeOut = redirectStore(cache) // into webscoket
  }

  onmessage(msg) {
    this.storeIn.setData(msg)
  }

  subscribeIn(fn, type = '') {
    if (type !== '') {
      const sub = this.storeIn.subject.pipe(
        filter((msg) => {
          if (msg.type === undefined) {
            return false
          }
          return msg.type === type
        })
      )
      return sub.subscribe(fn)
    }
    return this.storeIn.subscribe(fn)
  }

  subscribeOut(fn, type = '') {
    if (type !== '') {
      const sub = this.storeOut.subject.pipe(
        filter((msg) => {
          if (msg.type === undefined) {
            return false
          }
          return msg.type === type
        })
      )
      return sub.subscribe(fn)
    }
    return this.storeOut.subscribe(fn)
  }

  onclose() {}

  send(data) {
    return this.storeOut.setData(data)
  }

  publishState(loading = false, connected = false, url="") {
    this.storeOut.setData({
      type: '_ws',
      action: 'state',
      payload: {
        loading,
        connected,
        url
      }
    })
  }

  get init() {
    return initialState
  }

  connect(url = 'ws://localhost:8442') {
    this.storeOut.setData({
      type: '_ws',
      action: 'connect',
      payload: {
        url
      }
    })
  }

  disconnect() {
    this.storeOut.setData({
      type: '_ws',
      action: 'disconnect',
      payload: {}
    })
  }
}
