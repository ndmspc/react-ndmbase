import { useEffect } from 'react';
import { useNdmSpcLocal, useNdmSpcPlugin, useNdmSpcRedirect } from '../../'

export const PluginService = () => {

  const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws,] = useNdmSpcLocal("ndmspcZmq2ws");
  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [ndmspcConfiguration, ndmspcConfigurationStore] = useNdmSpcRedirect("ndmspcConfiguration");
  const ndmspcCurrentPlugin = useNdmSpcPlugin()

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return
    const subscriptionJob = ndmspcCurrentPlugin.createExternalSubscription(
      "job",
      (data) => {
        console.log("job", data)
        if (!data) return
        const { res, extra, error = null } = data
        if (error) {
          ndmspcAppStore.setData({ ...ndmspcApp, isZmq2wsOpen: true });
          // console.log(error, extra)
        } else if (res) {
          ndmspcAppStore.setData({ ...ndmspcApp, isJobMonitorOpen: true, jobMonitorMgr: res?.manager, jobMonitorJob: res?.job, total: 0, jobExtra: extra });
        }
      },
      null
    );

    const subscriptionPluginPipe = ndmspcCurrentPlugin.createExternalSubscription(
      "pluginPipe",
      (data) => {
        if (!data) return
        console.log("pluginPipe", data)
        if (data.type === "plugin" && data.action === "config" && data?.payload?.err) {
          if (ndmspcApp) {
            ndmspcAppStore.setData({ ...ndmspcApp, isPluginConfigurationOpen: true });
          }
        }
      },
      null
    );

    ndmspcCurrentPlugin.getDistributorById("cluster")?.sendInputData(ndmspcZmq2ws);
    return () => {
      subscriptionJob?.unsubscribe();
      subscriptionPluginPipe?.unsubscribe();
      ndmspcCurrentPlugin.getDistributorById("job")?.sendOutputData(null);
    }
  }, [ndmspcCurrentPlugin]);


  useEffect(() => {
    if (!ndmspcZmq2ws) return
    if (ndmspcCurrentPlugin)
      ndmspcCurrentPlugin.getDistributorById("cluster")?.sendInputData(ndmspcZmq2ws);
  }, [ndmspcZmq2ws]);

  useEffect(() => {
    if (!ndmspcConfiguration) return
    if (!ndmspcCurrentPlugin) return
    if (ndmspcCurrentPlugin && ndmspcConfiguration[ndmspcCurrentPlugin.state.name]) {

      const { applyPlugin = true, applyAnalysis = true, ...configuration } = ndmspcConfiguration
      // console.log(applyPlugin, applyAnalysis, configuration)
      if (applyPlugin) ndmspcCurrentPlugin.getDistributorById("configuration")?.sendInputData(configuration[ndmspcCurrentPlugin.state.name][configuration[ndmspcCurrentPlugin.state.name].current]);
      if (applyAnalysis) {
        const analysis = ndmspcAnalyses.analyses[ndmspcAnalyses.analysis]
        ndmspcAnalysesStore.setData({ ...ndmspcAnalyses, analyses: { ...ndmspcAnalyses.analyses, [analysis.name]: { ...ndmspcAnalyses?.analyses[analysis.name], plugin: ndmspcCurrentPlugin.state.name, plugins: configuration } } });
      }
    }
  }, [ndmspcConfiguration]);

  useEffect(() => {
    if (!ndmspcCurrentPlugin) return
    if (!ndmspcAnalyses) return
    if (!ndmspcAnalyses?.analysis) return
    // if (ndmspcConfiguration?.init) return
    const analysis = ndmspcAnalyses?.analyses[ndmspcAnalyses.analysis]
    if (analysis && analysis?.plugins) {
      // console.log("Loading Annalysis plugins", ndmspcAnalyses?.analyses[ndmspcAnalyses.analysis].plugins)
      const configuration = analysis.plugins
      if (!ndmspcConfiguration) ndmspcConfigurationStore.setData({ ...configuration });
    }
  }, [ndmspcAnalyses]);

  return null
};
