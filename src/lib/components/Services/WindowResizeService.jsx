import { useEffect, useState } from 'react';
import { useNdmSpcRedirect } from '../../'

function getWindowSize() {
  const { innerWidth, innerHeight } = window;
  return { innerWidth, innerHeight };
}

export const WindowResizeService = () => {
  const [windowSize, setWindowSize] = useState(getWindowSize());
  const [, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");

  useEffect(() => {
    function handleWindowResize() {
      setWindowSize(getWindowSize());
    }
    window.addEventListener('resize', handleWindowResize);
    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, []);

  useEffect(() => {
    console.log("Internal window size : ", windowSize.innerWidth, windowSize.innerHeight)
    ndmspcInternalStore.setData({ type: "ui", action: "resize" });
  }, [windowSize]);

  return null;
};
