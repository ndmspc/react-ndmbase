import { useState, useEffect, useContext } from 'react';
import { NdmSpcContext, useStreamBrokerIn, useNdmSpcLocal, useNdmSpcRedirect, isObjectEmpty, transformSubmitUrl } from '../../'

export const Zmq2wsSubscriptionService = ({ name = "zmq2ws" }) => {

  const [subscriptions, setSubscriptions] = useState([]);
  const [node, setNode] = useState(null);
  const [jobInfo, setJobInfo] = useState(null);
  const zmq = useStreamBrokerIn("zmq", name);
  const [ndmspcClusterSubscribe,] = useNdmSpcRedirect("ndmspcClusterSubscribe");
  const [, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  // const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws, ndmspcZmq2wsStore] = useNdmSpcLocal("ndmspcZmq2ws");
  const [, ndmspcNodeStore] = useNdmSpcRedirect("ndmspcNode");
  const [, ndmspcJobsStore] = useNdmSpcRedirect("ndmspcJobs");
  const sb = useContext(NdmSpcContext)[name];

  useEffect(() => {

    if (ndmspcClusterSubscribe && Object.keys(ndmspcClusterSubscribe).length) {
      // console.log(ndmspcClusterSubscribe);

      const current = `${ndmspcClusterSubscribe.name}|${ndmspcClusterSubscribe.sub}|${ndmspcClusterSubscribe.src}`
      if (ndmspcClusterSubscribe.action === "subscribe") {
        if (subscriptions.filter(a => a === current).length === 0) {
          ndmspcZmq2wsStore.setData({ ...ndmspcZmq2ws, sub: current });
          setSubscriptions([...subscriptions, current])
          sb.send({
            type: "sub",
            name: current.split('|')[0],
            sub: current.split('|')[1],
            src: current.split('|')[2]
          });
          // console.log(ndmspcClusterSubscribe);
        }
      }
      else if (ndmspcClusterSubscribe.action === "unsubscribe") {
        if (subscriptions.filter(a => a === current).length > 0) {
          setSubscriptions(subscriptions.filter(a => a !== current))
          // ndmspcZmq2wsStore.setData({ ...ndmspcZmq2ws, sub: null });
          sb.send({
            type: "unsub",
            name: current.split('|')[0],
            sub: current.split('|')[1],
            src: current.split('|')[2]
          });
          // console.log(ndmspcClusterSubscribe);
        }
      }
      else if (ndmspcClusterSubscribe.action === "subscribe-all") {
        // console.log("Doing subscribe-all")
        for (const current of subscriptions) {
          sb.send({
            type: "sub",
            name: current.split('|')[0],
            sub: current.split('|')[1],
            src: current.split('|')[2]
          });
        }
      }
      else if (ndmspcClusterSubscribe.action === "unsubscribe-all") {
        // console.log("Doing subscribe-all")
        for (const current of subscriptions) {
          sb.send({
            type: "unsub",
            name: current.split('|')[0],
            sub: current.split('|')[1],
            src: current.split('|')[2]
          });
        }
        // setSubscriptions([])
      }
      else if (ndmspcClusterSubscribe.action === "subscribe-clear") {
        // console.log("Doing subscribe-clear")
        setSubscriptions([])
      }
    }


  }, [ndmspcClusterSubscribe]);

  useEffect(() => {
    if (!zmq || isObjectEmpty(zmq)) {
      setNode(null)
      setJobInfo(null)
      return
    }
    console.log(zmq);
    // console.log(zmq?.payload?.data?.node)
    setNode(zmq?.payload?.data?.node)
    setJobInfo({ mgr: zmq?.payload?.sub, jobs: zmq?.payload?.data?.jobs })

  }, [zmq]);

  useEffect(() => {

    if (node) {
      ndmspcNodeStore.setData({ ...node, executorUrl: ndmspcZmq2ws?.executorUrl })

      const url = transformSubmitUrl(node.submitUrl)
      // console.log(url, ndmspcZmq2ws?.submitUrl, node.slots, ndmspcZmq2ws?.slots)
      if (url !== ndmspcZmq2ws?.submitUrl || node.slots !== ndmspcZmq2ws?.slots) {
        console.log("Udating ndmspcZmq2ws ", url, node.slots)
        ndmspcZmq2wsStore.setData({ ...ndmspcZmq2ws, submitUrl: url, slots: node.slots });
      }
    } else {
      ndmspcZmq2wsStore.setData({ ...ndmspcZmq2ws, submitUrl: null, slots: 0 });
      ndmspcNodeStore.setData(null)
    }
  }, [node]);

  useEffect(() => {
    // console.log(jobInfo)
    ndmspcJobsStore.setData(jobInfo)

  }, [jobInfo]);

  useEffect(() => {
    if (subscriptions.length === 0)
      ndmspcInternalStore.setData({ type: "zmq2ws", action: "unsubscribed" });
    else
      ndmspcInternalStore.setData({ type: "zmq2ws", action: "subscribed" });

  }, [subscriptions]);

  // useEffect(() => {
  //   console.log("Zmq2wsSubscriptionService rerender");
  //   // ndmspcClusterSubscribeStore.setData({ type: "ui", action: "resize" });
  // });

  return null;
};
