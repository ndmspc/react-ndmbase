export default class AppGlobalScope {
  state = {}
  distributors = new Map()
  handlerFunc = new Map()
  subscriptions = new Map()

  constructor(state) {
    this.state = state ? state : {}
  }

  addDistributor(distributor) {
    this.distributors.set(distributor.getId(), distributor)
  }

  addHandlerFunc(
    id,
    callbackInputFunc = null,
    callbackCustomFunc = null
  ) {
    this.handlerFunc.set(id, {
      callbackInputFunc: callbackInputFunc,
      callbackCustomFunc: callbackCustomFunc
    })
  }

  getDistributorById(id) {
    const d = this.distributors.get(id)
    if (d === undefined) return null
    return d
  }

  addSubscription(id) {
    if (this.subscriptions.get(id)) {
      this.clearSubscription(id)
      console.log('unsubscribe')
    }
    const sub = this.createSubscription(id)
    this.subscriptions.set(id, sub)
  }

  createExternalSubscription(
    id,
    callbackOutputFunc = null,
    callbackCustomFunc = null
  ) {
    const d = this.getDistributorById(id)
    if (!d) return null
    return d.getObservableInstance()
      .subscribe((receivedData) => {
        if (receivedData.flag === 'OUT') {
          if (callbackOutputFunc) callbackOutputFunc(receivedData.data)
        } else {
          if (callbackCustomFunc) callbackCustomFunc(receivedData.data)
        }
      })
  }

  createSubscription(id) {
    const handlerFunc = this.handlerFunc.get(id)
    const d = this.getDistributorById(id)
    if (!d) return null
    return d.getObservableInstance()
      .subscribe((receivedData) => {
        if (receivedData.flag === 'IN') {
          if (handlerFunc?.callbackInputFunc) handlerFunc.callbackInputFunc(receivedData.data)
        } else {
          if (handlerFunc?.callbackCustomFunc) handlerFunc.callbackCustomFunc(receivedData.data)
        }
      })
  }

  addAllSubscriptions() {
    this.distributors.forEach((_, id) => {
      this.addSubscription(id)
    })
  }

  clearSubscription(id) {
    this.subscriptions.get(id)?.unsubscribe()
  }

  clearAllSubscriptions() {
    this.distributors.forEach((_, id) => this.clearSubscription(id))
  }
}
