import { ReplaySubject } from 'rxjs'

export default class Distributor {
  subject
  id

  constructor(id) {
    this.id = id
    this.subject = new ReplaySubject(1)
  }

  sendOutputData(data) {
    this.subject.next({ data: data, flag: 'OUT' })
  }

  sendInputData(data) {
    this.subject.next({ data: data, flag: 'IN' })
  }

  sendCustomData(data) {
    this.subject.next({ data: data })
  }

  getId() {
    return this.id
  }

  getObservableInstance() {
    return this.subject.asObservable()
  }
}
