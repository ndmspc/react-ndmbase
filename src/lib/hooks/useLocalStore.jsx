import { useLayoutEffect, useState } from 'react'

export default (store) => {
  const [local, setLocal] = useState(store.initialState)
  useLayoutEffect(() => {
    store.init()
    const subs = store.subscribe(setLocal)
    return () => subs.unsubscribe()
  }, [])

  return local
}
