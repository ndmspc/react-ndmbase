import { useContext } from 'react';
import { NdmSpcContext, useRedirectStore, useLocalStore, isObjectEmpty } from "../";

export const useNdmSpcLocal = (name = "ndmspcApp") => {
  const store = useContext(NdmSpcContext)[name]
  const local = useLocalStore(store)
  return [local?.data, store]
}

export const useNdmSpcRedirect = (name = "ndmspcApp") => {
  const store = useContext(NdmSpcContext)[name]
  const local = useRedirectStore(store)
  if (isObjectEmpty(local)) return [null, store]
  return [local, store]
}

export const useNdmSpcJobMontitor = (name = "ndmspcJobs") => {
  const store = useContext(NdmSpcContext)[name]
  const local = useRedirectStore(store)
  if (isObjectEmpty(local)) return [null, store]
  return [local, store]
}

export const useNdmSpcPlugins = (plugins = "ndmspcPlugins") => {
  return useContext(NdmSpcContext)[plugins]
}

export const useNdmSpcPlugin = (plugin = "EmptyPlugin", plugins = "ndmspcPlugins") => {
  const [ndmspcAnalyses,] = useNdmSpcLocal("ndmspcAnalyses");
  if (ndmspcAnalyses?.plugin) {
    plugin = ndmspcAnalyses?.plugin
  }
  const p = useContext(NdmSpcContext)[plugins][plugin]
  if (p === undefined) return null
  return p
}