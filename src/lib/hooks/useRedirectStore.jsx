import { useLayoutEffect, useState } from 'react'

export default (store) => {
  const [local, setLocal] = useState(store.initialState)
  useLayoutEffect(() => {
    const subs = store.subscribe(setLocal)
    // store.init()
    return () => subs.unsubscribe()
  }, [])

  return local
}
