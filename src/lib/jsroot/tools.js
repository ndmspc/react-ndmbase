import { openFile, createHistogram, redraw, cleanup } from "jsroot";

const displayImageOfProjection = async (
  idSourceElement,
  targetElementIds,
  width,
  height
) => {
  try {
    const sourceSvgElement =
      document.getElementById(idSourceElement).firstElementChild;
    const targetElements = targetElementIds.map((targetId) =>
      document.getElementById(targetId)
    );
    const canvas = document.createElement("canvas");
    // set width a height of elements
    sourceSvgElement.setAttribute("height", height);
    sourceSvgElement.setAttribute("width", width);
    canvas.setAttribute("height", height);
    canvas.setAttribute("width", width);

    const svgString = new XMLSerializer().serializeToString(sourceSvgElement);
    const ctx = canvas.getContext("2d");
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    // eslint-disable-next-line no-undef
    const DOMURL = self.URL || self.webkitURL || self;
    // eslint-disable-next-line no-undef
    const img = new Image();
    // create object url of canvas using blob object
    // eslint-disable-next-line no-undef
    const svg = new Blob([svgString], { type: "image/svg+xml;charset=utf-8" });
    const url = DOMURL.createObjectURL(svg);

    // the function is executed after load url of the image
    img.onload = await function () {
      // draw png image and create url
      ctx.drawImage(img, 0, 0);
      const png = canvas.toDataURL("image/png");

      // targetElement.setAttribute('src', png)
      targetElements.forEach((element) => {
        if (element) {
          element.setAttribute("material", `src`, png);
        } else {
          console.log(`element ${element.getId()} not found`);
        }
      });

      //  this.binPngLocal.setData(png)
      DOMURL.revokeObjectURL(png);
    };
    img.src = url;
  } catch (e) {}
};

// TODO decide function usage
const openTH1Projection = async (
  projectionAxis,
  info,
  idTargetElm,
  axisArray,
  projections,
  projIndex
) => {
  const xBin = info.bin.xPos - 1;
  const yBin = info.bin.yPos - 1;
  try {
    const projection =
      projections.fFolders.arr[xBin].fFolders.arr[yBin].fFolders.arr[
        projIndex || 0
      ];
    await cleanup(idTargetElm);
    await redraw(idTargetElm, projection, "cols");
    await displayImageOfProjection(
      "projectionContainer",
      "th-mapping",
      "tablet",
      "desktop-tablet",
      "500px",
      "400px"
    );
  } catch (e) {
    console.log("you don't have this projection");
  }
};

// create projection which depends on selected axis
const createTH1Projection = async (
  projectionAxis,
  selectedData,
  idTargetElms,
  axisArray
) => {
  if (!selectedData || !projectionAxis) {
    return;
  }

  const projectionTargetAxis = projectionAxis === axisArray[0];
  const primary = projectionTargetAxis ? axisArray[0] : axisArray[1];
  const secondary = projectionTargetAxis ? axisArray[1] : axisArray[0];

  const primaryAxis = `f${secondary}axis`;
  const secondaryAxis = `f${primary}axis`;
  const binAxis = `bin${primary.toLowerCase()}`;

  const h1 = await createHistogram(
    "TH1I",
    selectedData.obj[primaryAxis].fNbins
  );

  // fill important histogram info
  h1.fXaxis.fXmin = selectedData.obj[primaryAxis].fXmin;
  h1.fXaxis.fXmax = selectedData.obj[primaryAxis].fXmax;
  h1.fXaxis.fTitle = selectedData.obj[primaryAxis].fTitle;
  h1.fXaxis.fLabels = selectedData.obj[primaryAxis].fLabels;

  const currLabel =
    selectedData.obj[secondaryAxis].fLabels?.arr.length > 0
      ? selectedData.obj[secondaryAxis].fLabels.arr[selectedData[binAxis] - 1]
          .fString
      : "";

  for (let n = 0; n < selectedData.obj[primaryAxis].fNbins; n++) {
    h1.setBinContent(
      n + 1,
      projectionAxis === axisArray[0]
        ? selectedData.obj.getBinContent(selectedData[binAxis], n + 1)
        : selectedData.obj.getBinContent(n + 1, selectedData[binAxis])
    );
  }
  h1.fName = "px";
  h1.fTitle =
    `Projection of bin=${projectionAxis}` +
    (selectedData.binName !== ""
      ? selectedData.name
      : selectedData[binAxis] - 1) +
    `[` +
    currLabel +
    `]`;
  for (const idTargetElm of idTargetElms) {
    await cleanup(idTargetElm);
    await redraw(idTargetElm, h1, "cols");
  }
};

// read projections from root files
const readProjectionFromFile = async (
  filename,
  basePath,
  ix,
  iy,
  projName,
  targetElmIds
) => {
  const rootFile = await openFile(filename);
  const proj = await rootFile.readObject(`${basePath}/${ix}/${iy}/${projName}`);
  const painters = new Map();
  for (const targetElmId of targetElmIds) {
    await cleanup(targetElmId);
    const hPainter = await redraw(targetElmId, proj, "colz");
    painters.set(targetElmId, hPainter);
  }
  return painters;
};

// create context menu for displayed histogram
const appendContextMenu = async (
  histogramPainter,
  projNames,
  operations,
  callbackFunc
) => {
  if (histogramPainter && projNames.length) {
    histogramPainter.oldFillHistContextMenu =
      histogramPainter.fillHistContextMenu;

    histogramPainter.fillHistContextMenu = function (menu) {
      const tip = menu.painter.getToolTip(menu.getEventPosition());

      menu.add(`sub:Histogram bin [${tip.binx}, ${tip.biny}]`);
      projNames.forEach((child) => {
        menu.add(`sub:Projection ${child}`);
        operations.forEach((operation) => {
          menu.add(`Operation ${operation}`, () => {
            const data = {
              projName: child,
              operation: operation,
              biny: tip.biny,
              binx: tip.binx,
            };
            callbackFunc(data);
          });
        });
        menu.add("endsub:");
      });
      menu.add("endsub:");
      return this.oldFillHistContextMenu(menu);
    };
  }
};

// create TH2 demo histogram
const createTH2DemoHistogram = (
  xLength,
  yLength,
  initialContent,
  contentIncrement
) => {
  const histo = createHistogram("TH2I", xLength, yLength);
  let cnt = initialContent;
  for (let iy = 1; iy <= 20; iy++)
    for (let ix = 1; ix <= 20; ix++) {
      const bin = histo.getBin(ix, iy);
      let val = 0;
      val = cnt;
      histo.setBinContent(bin, val);
      cnt += contentIncrement;
    }

  histo.fXaxis.fTitle = "x Axis";
  histo.fYaxis.fTitle = "y Axis";
  histo.fName = "You don't have a valid file path";
  histo.fTitle = "This is a TH2 histogram demo";
  histo.fMaximum = cnt;

  return histo;
};

// create TH3 demo histogram
const createTH3DemoHistogram = (
  xLength,
  yLength,
  zLength,
  initialContent,
  contentIncrement
) => {
  const histo = createHistogram("TH3I", xLength, yLength, zLength);
  let cnt = initialContent;

  for (let iz = 1; iz <= 20; iz++)
    for (let iy = 1; iy <= 20; iy++)
      for (let ix = 1; ix <= 20; ix++) {
        const bin = histo.getBin(ix, iy, iz);
        let val = 0;
        val = cnt;
        histo.setBinContent(bin, val);
        // cnt += 0.0005
        cnt += contentIncrement;
      }

  histo.fXaxis.fTitle = "x Axis";
  histo.fYaxis.fTitle = "y Axis";
  histo.fZaxis.fTitle = "z Axis";
  histo.fName = "You don't have a valid file path";
  histo.fTitle = "This is a TH3 histogram demo";
  histo.fMaximum = cnt;

  return histo;
};

export {
  createTH1Projection,
  openTH1Projection,
  displayImageOfProjection,
  readProjectionFromFile,
  appendContextMenu,
  createTH2DemoHistogram,
  createTH3DemoHistogram,
};
