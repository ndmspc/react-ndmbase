/* global localStorage */
import { ReplaySubject } from 'rxjs'

const initialState = {
  selectedBin: null,
  options: []
}

export const executorStore = (
  key,
  state = initialState,
  subject = new ReplaySubject(1)
) => ({
  init: (_key = key) => {
    const data = localStorage.getItem(_key)
    if (data === null) {
      state = { selectedBin: null, options: [] }
      localStorage.setItem(_key, JSON.stringify(state))
    }
  },
  subscribe: (setState) => subject.subscribe(setState),
  setOptions: (options) => {
    const state = JSON.parse(localStorage.getItem(key))
    localStorage.setItem(
      key,
      JSON.stringify({
        ...state,
        options: options
      })
    )
  },
  executeFunction: (functionIndex, args) => {
    subject.next({ index: functionIndex, args: args })
  },
  getOptions: () => {
    const state = JSON.parse(localStorage.getItem(key))
    return state.options ? state.options : []
  },
  setSelectedBin: (binData) => {
    const state = JSON.parse(localStorage.getItem(key))
    localStorage.setItem(
      key,
      JSON.stringify({
        ...state,
        selectedBin: binData
      })
    )
  },
  getSelectedBin: () => {
    const state = JSON.parse(localStorage.getItem(key))
    return state.selectedBin ? state.selectedBin : null
  },
  clear: () => {
    state = {
      selectedBin: null,
      options: []
    }
    localStorage.setItem(key, JSON.stringify(state))
  },
  clearLocalStorage: () => {
    localStorage.removeItem(key)
  },
  initialState
})
