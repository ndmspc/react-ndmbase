import { openFile, redraw, displayImageOfProjection, urlExists } from "../";

export const getBinId = (data, prefix = "local") => {
    // console.log(data)

    const r = {
        id: `${prefix}-${data.id?.replace("-", "_") ??
            data.histogramId?.replace("-", "_") ??
            "unknown"
            }-${data.binx ?? data.x ?? 1}-${data.biny ?? data.y ?? 1}-${data.binz ?? data.z ?? 1
            }`,
        data: data,
    };
    // console.log(r)
    return r;
};



export const readJsrootObjectFromFile = async (fileName, projName, ok = null, fail = null) => {
    if (await urlExists(fileName)) {
        const rootFile = await openFile(fileName);
        // console.log(rootFile)
        if (rootFile) {
            const proj = await rootFile.readObject(projName);
            if (proj) {
                // console.log(proj)
                if (ok) ok(proj)
            } else {
                if (fail) fail(rootFile, null)
            }
        }
    } else {
        console.log(`File '${fileName}' doesn't exists`)
        if (fail) fail(null, null)
    }
}

export const getFileObjectNames = async (filename, ndmspcPlugin = null, distributor = "binObjects") => {
    // console.log(ndmspcPlugin)
    const rootFile = await openFile(filename).catch((err) => {
        if (ndmspcPlugin) ndmspcPlugin.getDistributorById(distributor)?.sendOutputData(null);
    });
    if (rootFile) {
        // console.log(rootFile)
        const objectNames = rootFile?.fKeys
            .filter(
                (key) => key.fClassName !== "TDirectory" && key.fClassName !== "TList" && !key.fClassName.startsWith("THn")
            )
            .map((key) => key.fName);

        // console.log(objectNames)
        if (ndmspcPlugin)
            ndmspcPlugin.getDistributorById(distributor)?.sendOutputData({ names: objectNames, filename: filename });
        else
            console.log(objectNames);
    }
};

export const drawJsrootObjectsFromFile = async (drawConfig, isVR = false, ndmspcPlugin = null, distributor = null) => {
    for (let fIndex in drawConfig) {
        const f = drawConfig[fIndex]
        const rootFile = await openFile(f.filename).catch((err) => {
            if (ndmspcPlugin && distributor) ndmspcPlugin.getDistributorById(distributor)?.sendOutputData(null);
        });
        // console.log(rootFile)
        if (rootFile) {
            const objects = f.objects;
            for (let oIndex in objects) {
                const o = objects[oIndex];
                const drawObj = await rootFile.readObject(o.name);
                if (drawObj) {
                    let painter = await redraw(o.divid, drawObj, o?.drawOpt ?? "");
                    if (painter) {
                        if (o?.buildLegend) painter.getPadPainter().buildLegend();
                        if (isVR && o?.vr) {
                            await displayImageOfProjection(
                                o.divid,
                                ["vr_proj"],
                                "800px",
                                "800px"
                            );
                        }
                    }
                }
            }
        }
    }
};
