import StorageService from './storageService'

export default class HistogramStorageService {
  constructor(storage, store) {
    this.storageService = new StorageService(storage, store)
  }

  getObjects() {
    return this.storageService.getObjects()
  }

  updateStore(updatedData) {
    this.storageService.updateStore(updatedData)
  }

  addObject(object) {
    const objectExists = this.storageService.getFirstObjectWith2Depth(
      'histogram',
      'name',
      object.histogram.name
    )
    if (objectExists) {
      this.storageService.updateObjectWith2Depth('histogram', 'name', object)
    } else {
      this.storageService.addObject(object)
    }
  }

  deleteObject(object) {
    this.storageService.deleteObjectWith2Depth('histogram', 'name', object)
  }

  getSortedObjects(sortOption, attr, sortedAttr) {
    return this.storageService.getSortedObjectsWith2Depth(
      sortOption,
      attr,
      sortedAttr
    )
  }
}
