import { httpRequest, transformSubmitUrl } from '../index'

export const submitSalsaJob = async (url, commands, submitUrl, cfg) => {
  if (!submitUrl) {
    console.log("submitSalsaJob :  No submit url")
  }

  submitUrl = transformSubmitUrl(submitUrl)

  console.log(cfg)
  const { ndmspc } = cfg;
  console.log(ndmspc)
  const body = {
    type: "salsa",
    subtype: "feeder",
    salsa: {
      host: submitUrl,
      job_type: "commands",
    },
    command: "sleep",
    args: "1",
    numberOfTasks: 1,
    indexes: null,
    bins: null,
    commands,
    xrdlogtype: ndmspc?.log?.type || "error-only",
    xrdlogdir: ndmspc?.log?.dir || "root://eos.ndmspc.io//eos/ndmspc/scratch/ndmspc/logs"
  };
  console.log(body)
  return await httpRequest(url, body)
};
