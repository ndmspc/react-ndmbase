export default class StorageService {
  constructor(storage, store) {
    this.storage = storage
    this.store = store
  }

  setStore(storage, store) {
    this.storage = storage
    this.store = store
  }

  getObjects() {
    return this.storage.data ? JSON.parse(this.storage.data) : []
  }

  updateStore(updatedData) {
    this.store.setData(JSON.stringify(updatedData))
  }

  addObject(object) {
    const parsedStore = this.getObjects()
    const parsedObject = [...parsedStore, object]
    this.updateStore(parsedObject)
  }

  updateObjectWith2Depth(attr, attr2, newObject) {
    const parsedObject = this.getObjects().map((object) =>
      object[attr][attr2] === newObject[attr][attr2] ? newObject : object
    )
    this.updateStore(parsedObject)
  }

  deleteObjectWith2Depth(attr, attr2, objectToDelete) {
    const parsedStore = this.getObjects()
    const updatedObjectArr = parsedStore.filter(
      (object) => object[attr][attr2] !== objectToDelete[attr][attr2]
    )
    this.updateStore(updatedObjectArr)
  }

  deleteObjectWith1Depth(attr, objectToDelete) {
    const parsedStore = this.getObjects()
    const updatedObjectArr = parsedStore.filter(
      (object) => object[attr] !== objectToDelete[attr]
    )
    this.updateStore(updatedObjectArr)
  }

  getFirstObjectWith2Depth(attr, attr2, value) {
    const objects = this.getObjectsWith2Depth(attr, attr2, value)
    if (objects.length > 0) {
      return objects[0]
    }
    return null
  }

  getFirstObjectWith1Depth(attr, value) {
    const objects = this.getObjectsWith1Depth(attr, value)
    if (objects.length > 0) {
      return objects[0]
    }
    return null
  }

  getObjectsWith2Depth(attr, attr2, value) {
    return this.getObjects().filter((object) => object[attr][attr2] === value)
  }

  getObjectsWith1Depth(attr, value) {
    return this.getObjects().filter((object) => object[attr] === value)
  }

  getSortedObjectsWith2Depth(sortOption, attr, sortedAttr) {
    if (sortOption === 'ASC' || sortOption === 'asc') {
      return this.getObjects().sort((a, b) =>
        a[attr][sortedAttr] < b[attr][sortedAttr] ? 1 : -1
      )
    } else if (sortOption === 'DESC' || sortOption === 'desc') {
      return this.getObjects().sort((a, b) =>
        a[attr][sortedAttr] < b[attr][sortedAttr] ? -1 : 1
      )
    } else {
      return this.getObjects()
    }
  }

  getSortedObjectsWith1Depth(sortOption, sortedAttr) {
    if (sortOption === 'ASC' || sortOption === 'asc') {
      return this.getObjects().sort((a, b) => a[sortedAttr] < b[sortedAttr])
        ? 1
        : -1
    } else if (sortOption === 'DESC' || sortOption === 'desc') {
      return this.getObjects().sort((a, b) => a[sortedAttr] < b[sortedAttr])
        ? -1
        : 1
    } else {
      return this.getObjects()
    }
  }

  containObjectWith2Depth(attr, attr2, value) {
    const objects = this.getObjects().filter(
      (object) => object[attr][attr2] === value
    )
    if (objects.length > 0) {
      return true
    } else {
      return false
    }
  }

  containObjectWith1Depth(attr, value) {
    const objects = this.getObjects().filter((object) => object[attr] === value)
    if (objects.length > 0) {
      return true
    } else {
      return false
    }
  }
}
