import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    chunkSizeWarningLimit: "1M",
  },
  plugins: [react()],
  optimizeDeps: {
    exclude: ['gl > gl']
  }
})
