import { resolve } from 'path'
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({

  server: {
    // cors: false,
    proxy: {
      '/auth': {
        target: 'https://keycloak.192.168.49.2.nip.io',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/auth/, ''),
      },
    }
  },

  build: {
    lib: {
      entry: resolve(__dirname, 'src/lib/index.jsx'),
      name: 'React Library Vite',
      fileName: (format) => `react-ndmspc-core.${format}.js`
    },
    rollupOptions: {
      // externalize deps that shouldn't be bundled
      // into your library
      external: ['react', 'react-dom', '@textea/json-viewer', '@textea/json-viewer', 'axios', 'rxjs', 'jsroot'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          react: 'React',
          rxjs: 'rxjs',
          jsroot: 'jsroot'
        },
        inlineDynamicImports: true
      }
    }
  },
  plugins: [react()],
  optimizeDeps: {
    exclude: ['gl > gl']
  }
})
